package com.sbm.modules.consent.service.segment;

import com.sbm.modules.consent.dto.SegmentDto;

import java.util.List;

public interface SegmentService {

    public List<SegmentDto> getAllSegments();
}
